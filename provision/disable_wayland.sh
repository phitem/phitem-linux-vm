#!/usr/bin/env bash

# Fail à la 1ère erreur ou si variable unset
set -euo pipefail
# Affiche les commandes
set -x

grep -E "^WaylandEnable=false" /etc/gdm3/custom.conf || sudo sed -i '/^\[daemon\]/a WaylandEnable=false' /etc/gdm3/custom.conf 

systemctl restart gdm

