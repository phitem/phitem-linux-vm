#!/usr/bin/env bash

# Fail à la 1ère erreur ou si variable unset
set -euo pipefail
# Affiche les commandes
set -x

GITLAB_URL="https://gricad-gitlab.univ-grenoble-alpes.fr"
DESIGN_URL="${GITLAB_URL}/phitem/design-linux-uga/-/releases/permalink/latest"
DESIGN_REPO_URL="${GITLAB_URL}/api/v4/projects/11947/packages/generic/"


# Reset GDM theme
update-alternatives --auto gdm-theme.gresource

rm -f /etc/default/grub.d/50-cloudimg-settings.cfg # ubuntu vagrant boxes have this conf preventing plymouth splash to appear
update-grub

# Get latest design-linux-uga release version
design_uga_latest="$(curl -Ls -o /dev/null -w %{url_effective} $DESIGN_URL  | awk -F '/' '{print $NF}')"

# Install latest design-linux-uga version
wget -q "${DESIGN_REPO_URL}/design-linux-uga/${design_uga_latest}/design-linux-uga.deb" && apt-get -y install ./design-linux-uga.deb
rm design-linux-uga.deb

# Theme Yaru
apt-get install -y yaru-theme-gtk yaru-theme-gnome-shell yaru-theme-icon

# Modify default gnome-terminal background
# On remplace le violet ubuntu par #333333
echo 'echo -ne "\e]11;#1a1a1a\e\\"' >> /home/vagrant/.bashrc

# Configure Gnome
printf "user-db:user\nsystem-db:local" > /etc/dconf/profile/user
mkdir -p /etc/dconf/db/local.db
printf "
[org/gnome/desktop/interface]
gtk-theme='Yaru'
icon-theme='Yaru'

[org/gnome/desktop/background]
picture-uri='file:///usr/share/backgrounds/uga/uga-slideshow.xml'
primary-color='#2a2e46'
" > /etc/dconf/db/local.d/99-phitem
dconf update


# Use ubuntu-session : permet d'avoir le thème Yaru utilisé par Gnome Shell
apt-get install -y ubuntu-session

# Start / restart GDM
systemctl restart gdm

