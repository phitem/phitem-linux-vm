# Installations automatiques de VM Ubuntu avec Vagrant

*Objectif* : créer automatiquement des VM Virtualbox pour les étudiants PhITEM.

*Statut* : stable

## Prérequis

 * Vagrant : https://www.vagrantup.com/
 * Virtualbox (dernière version) : https://www.virtualbox.org/

## Exemple d'utilisation

Télécharger le [zip](https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/phitem-linux-vm/-/archive/master/phitem-linux-vm-master.zip) et l'extraire.

Dans le dossier contenant le fichier `Vagrantfile`, lancer :

```bash
vagrant up
```
> Sous *Windows*, vous pouvez également double-cliquer sur le fichier `Start.bat`.

Le login / mot de passe par défaut est : `vagrant / vagrant`


Pour supprimer la VM :
```bash
vagrant destroy
```
> Sous *Windows*, vous pouvez également double-cliquer sur le fichier `Reset.bat`.

## Informations

Vagrant va créer une machine virtuelle dans VirtualBox. Il est ensuite possible de ne plus passer par Vagrant pour l'utiliser.

### Installer des logiciels
La machine virtuelle crée intègre l'outil `ph-install` qui vous permet d'installer des logiciels avec la même configuration qu'à PhITEM.

Par exemple, dans un terminal :

```bash
ph-install libreoffice
```

La liste des logiciels disponibles est [ici](https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/phitem-linux-logiciels/-/blob/master/doc/liste_des_logiciels.md).

### Partager des fichiers avec l'hôte

Vous pouvez copier des fichiers de la machine virtuelle à l'hôte en les copiant dans le dossier `Hôte` de la machine virtuelle. Ils seront mis dans le dossier contenant le fichier `Vagrantfile` sur le PC hôte.
